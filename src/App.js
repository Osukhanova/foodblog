import React, { Component } from 'react'

import {Route,Switch, BrowserRouter as Router,} from 'react-router-dom'
import { omit } from 'lodash'

import Header from "./App/Header/Header"
import Main from "./App/Main/Main"
import Footer from "./App/Footer/Footer"

import AboutPage from "./App/Main/AboutPage/AboutPage"
import Gallery from "./App/Main/Gallery/Gallery"
import Foods from "./App/Main/Foods/Foods"
import Contacts from "./App/Main/Contacts/Contacts"
import PeoplePage from "./App/Main/PeoplePage/PeoplePage"
import Favorites from "./App/Main/Favorites/Favorites"


import './common/style/reset.css'
import './common/style/grid.css'
import './common/style/base.css'



class App extends Component {
 
    state = {
		photoInFavorites: {
			// 1: '',
		},

		likePhotoState: {
			1:"",
			2:"",
		},

		choosePhotoSection:{
            1:"",
			2:"",
			3:"",
			4:"",
		}
	}

      

	addProductToFav = (photoId) => {
		this.setState((prevState) => ({
			photoInFavorites: {
				...prevState.photoInFavorites,
				[photoId]: (prevState.photoInFavorites[photoId]) 
			}
		}))

	}

		removeProductFromFav = (photoId) => {
		this.setState((prevState) => ({
			photoInFavorites: omit(prevState.photoInFavorites, photoId)
		}))
	}
	
	
  addLike = (photoId) => {
	this.setState((prevState) => ({
		likePhotoState: {
			...prevState.likePhotoState,
			[photoId]: true
		}
	}))

}

  removeLike = (photoId) => {
	this.setState((prevState) => ({
		likePhotoState: {
			...prevState.likePhotoState,
			[photoId]: false
		}
	}))

}

      render() {



	return (
		<Router>
		<div>
 
			<Header />

		
			<div>
			<Switch>
                <Route  exact  path="/" component={Main}/> 
                <Route path="/about" component={AboutPage}/> 
                <Route path="/foods" component={Foods}/>   
                <Route path="/gallery" render = {() => (
					    <Gallery
						addProductToFav={this.addProductToFav}
						removeProductFromFav={this.removeProductFromFav}
						likePhotoState={this.state.likePhotoState}
						addLike={this.addLike}
						removeLike={this.removeLike}
						/>
					 
	 
					 )}/>
						
				
				
				
				<Route path="/favorites"  render = {() => (
                   <Favorites
				   addProductToFav={this.addProductToFav}
				   photoInFavorites={this.state.photoInFavorites}
				   likePhotoState={this.state.likePhotoState}
				   addLike={this.addLike}
				   removeLike={this.removeLike}
				   />
				

				)}/>
					
                <Route path="/contacts" component={Contacts}/>  
				<Route path="/photo/:photoId" component={PeoplePage}/>      
           </Switch>
		</div>

		<Footer />
			</div>
		
		</Router>
	)
}


}




export default App