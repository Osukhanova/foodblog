import React from 'react'
import {Link} from 'react-router-dom'
import './sliderList.css'

const SliderList = () => {
    return (
      <div> 
         <div className="slider-container">
                                    <h1 className="slider_capt">I am a blogger & I love foods</h1>
                                    <p className="text_slider">Lorem ipsum dolor sit amet, consecterur adipiscing elit. Sed
                                        nisi metus,
                                        tristique ndolor 
                                        non, ornare sagittis dolor. Nulla vestibulu lacus ...</p>
                                    <div className="button_section">
                                    <Link to="/about" className="read_more_button">
                                        <button className="view_more_button"> Read more &#10142; </button> </Link>
                                    </div>
                                </div>
                            </div> 
    )
}
export default SliderList