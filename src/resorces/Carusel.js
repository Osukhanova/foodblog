import React from 'react';
import Slider from "react-slick";

import slide_one from './05.jpg'
import slide_two from './bg_2.jpg'
import slide_three from './bg_1.jpg'

import SliderList from './SliderBlock/SliderList';

import "slick-carousel/slick/slick.css";
import "slick-carousel/slick/slick-theme.css";

import './slider.css'

const Carusel = () => {
    const settings = {
      dots: true,
      autoplay: true,
      autoplaySpeed: 2000,
      arrows: false,
      adaptiveHeight: true,
      pauseOnDotsHover: true,
      zindex: 10,
    };
return (
  <div>
       
  <section className="slider">

  			<div className="slider-block">
              <div  className="carrousel_wrapper">
              
              <Slider {...settings}>
              
         
  				<div className="slider-photo_block">
                  <div className="carrousel_image">
   					<img src={slide_one} alt="" className="slider_photo-1" />
   					<div className="SliderDescription">
   						<div className="container">
   							<div className="row slider-section">
  								<div className="col col-xs-12 col-md-12 col-sm-12 col-lg-12">
  									<SliderList />
  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
                  </div>
              
                 
                      <div className="slider-photo_block"> 
                  <div className="carrousel_image">
   				
   				<img src={slide_two} alt="" className="slider_photo-2" />
  					<div className="SliderDescription">
  						<div className="container">
  							<div className="row slider-section">
  								<div className="col  col-xs-12 col-md-12 col-sm-12 col-lg-12">
  									<SliderList />
  								</div>
  							</div>
  						</div>
  					</div>
  				</div>
                  </div>
                  
                
                  <div className="slider-photo_block">
                  <div className="carrousel_image">
					<img src={slide_three} alt="" className="slider_photo-3" />
					<div className="SliderDescription">
						<div className="container">
 							<div className="row slider-section">
 								<div className="col  col-xs-12 col-md-12 col-sm-12 col-lg-12">
									<SliderList />
 								</div>
							</div>
 						</div>
					</div>
 				</div>
                 </div>
           
     
      </Slider>

      </div>
  </div>

  </section>

  </div>
);
};

export default Carusel;