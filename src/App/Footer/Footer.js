import React from "react"
import './footer.css'
import SocialMenu from "./Social-menu-desc/Social-menu-desc"
import InformMenu from "./Inform-menu/Inform-menu"
import Copyright from "./Copyright/Copyright"

const Footer = () => {
	return (
		<section className="footer">
        <div className="container">
            <div className="row footer-row">
                <div className="col social_menu col-xs-12 col-md-6 col-sm-6 col-lg-6">
                  <SocialMenu />
                </div>
                <div className="col inform_menu col-xs-12 col-md-6 col-sm-6 col-lg-6">
                  <InformMenu/>
                </div>
            </div>
            <div className="row">
                <div className="col footerform col-xs-12 col-md-12 col-sm-12 col-lg-12">
				<Copyright /> 
                </div>
            </div>
        </div>
    </section>

	)
}

export default Footer 