import React from "react"
import address from '../footer-images/add.png'
import letter from '../footer-images/email.png'
import tel from '../footer-images/tel.png'

const InformMenu = () => {
	return (

		<div>
			<p>Have a Question?</p>
			<div className="info-block">
		
				<div className="adress">
				<img src={address} alt="" />
				</div>
				<p>55 Fake St. Fake fake, Toronto, Canada</p>
			</div>
			<div className="info-block">
		
				<div className="tel">
				<img src={letter} alt="" />
				</div>
				<p>+34 555 555 555</p>
			</div>
			<div className="info-block">
				<div className="email">
				<img src={tel} alt="" />
				</div>
				<p>info@gmail.com</p>
			</div>

		</div>
	)
}
export default InformMenu