import React from "react"
import './social-menu-desc.css'
import instagram from '../footer-images/icon.png'
import facebook from '../footer-images/facebook.png'
import twitter from '../footer-images/icon5.png'

const SocialMenu = () => {
	return (
       <div>
		<p>Stories.</p>
		<div className="social_menu_desc">
			<p>Far far away, behind the word mountains, far from the countries Vokalia and Consonantia, there live the blind texts.
			</p>
		</div>
	
	<div className="social-icons">
		<a href="https://www.instagram.com"   className="instagram">
		<img src={instagram} alt="" />
		</a>
		<a href="https://www.facebook.com" className="facebook">
		<img src={facebook} alt="" />
		</a>
		<a href="https://www.twitter.com"  className="twitter">
		<img src={twitter} alt="" />
		</a>
	</div>
      
	</div>	
				

    )
    }
    export default SocialMenu