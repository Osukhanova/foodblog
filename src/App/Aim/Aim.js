import React from "react"
import AimDesc from "./AimDesc/AimDesc"
import aimPhoto from "./aim-images/bg_3.jpg"

import './aim.css'


const Aim = () => {
	return (

    <section className="aim-section" id="#">
        <div id="transcroller-body" className="aos-all">
            <div className="aos-item" data-aos="fade-up">
                <div className="aim-photo">
                    <div className="aim-img">
                        <img src={aimPhoto} alt=""/>
                        </div>
                 
                    <div className="aim-block">
                        <div className="col aim_part_one col-xs-12 col-md-3 col-sm-12 col-lg-3">
                              <AimDesc/>
                 
                        </div>
                    </div>
                    </div>
                </div>
                </div>
    </section>

	)
}

export default Aim 