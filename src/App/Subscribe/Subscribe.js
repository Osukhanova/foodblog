import React from "react"
import './subscribe.css'
import SubscribeInfo from "./SubscribeInfo/SubscribeInfo"
import SubscribeForm from "./SubscribeForm/SubscribeForm"

const Subscribe = () => {
    return (
        <section className="subscribe-section" id="#">
            <div id="transcroller-body" className="aos-all">
                <div className="aos-item" data-aos="fade-up">
                    <div className="container">
                        <div className="row subscribe">
                            <div className="col subscribe_part_one col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                <SubscribeInfo />
                            </div>
                            <div className="col col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                <SubscribeForm />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default Subscribe