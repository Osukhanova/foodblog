import React from "react"
import Title from "./FeatureList/Title/Title"
import SideBar from "./FeatureList/SideBar/SideBar"
import FeaturesPhoto from "./FeatureList/FeaturesPhoto/FeaturesPhoto"
// import './features.css'

import '../Main/Gallery/Gallery.css'


const Features = () => {

    return (
        <section className="features-posts" id="#">
            <div id="transcroller-body" className="aos-all">
                <div className="aos-item" data-aos="fade-up">
                    <div className="container">
                        <div className="row features-posts-section">

                            <div className="col features_part_one col-xs-12 col-md-9 col-sm-12 col-lg-9">
                                <div className="row">
                                    <div className="col col-xs-12 col-md-12 col-sm-12 col-lg-12">

                                        <Title />

                                    </div>
                                </div>
                                <div className="row recent-stories_section">
                                    <div className="col col-xs-12 col-md-12 col-sm-12 col-lg-12">

                                        <FeaturesPhoto />

                                    </div>
                                </div>
                            </div>

                            <div className="col  col-xs-12 col-md-3 col-sm-12 col-lg-3">

                                <SideBar />

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </section>

    )
}

export default Features