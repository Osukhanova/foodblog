import React from 'react'
import FeaturesList from "./FeaturesList/FeaturesList"
import  featuresData from "./FeaturesData"
import '../../features.css'

const FeaturesPhoto = () => {
    return (
        <div>

            <div className="row recent-stories_section">
   
            {
            featuresData.map(({
              id,
              name,
              description,
              image,
            }) =>(

              <div className="col col-xs-12 col-md-4 col-sm-12 col-lg-4" key={id}>  
                <FeaturesList
                    name={name}
                    description={description}
                    image={image}
              />
            </div> 
            ))
          }
              </div>
             </div>
      
    )
}
export default FeaturesPhoto