import React from 'react'
// import '../../../features.css'

import '../../../../Main/Gallery/Gallery.css'

const FeaturesList = ({
    name,
    description,
    image,
}) => {
    return (
        <div>
            <div className="features_photos">
                <div className="features_photo">
                    <img src={image} alt="" />
                    <p>{name}</p>
                    <p>{description}</p>
                    <a href="foods.html">
                        <button>Read more &#10142; </button>
                    </a>
                </div>
            </div>
            </div>
            
    )
}
export default FeaturesList 