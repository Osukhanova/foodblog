import React from 'react'
import './sideBar.css'
import author from './images/author.jpg'
// import search from './images/icon1.png'

const SideBar = () => {
    return (
        <div>
            <div className="side-bar">
                <h3> About me</h3>
                <img src={author} alt="Athour" />
                <p>Hi! My name is Cathy Deon, behind the word mountains, far from the countries Vokalia
                and Consonantia, there live the blind texts. Separated they live in Bookmarksgrove
                                right at the coast of the Semantics, a large language ocean.</p>
                <div className="search-box">
                    <form action="">
                        <input type="text" placeholder="Search" className="search" />
                        <button className="search_button">
                        {/* <img src={search} alt="searchButton" />   */}
                        </button>
                    </form>
                </div>
            </div>
        </div>
    )
}
export default SideBar 