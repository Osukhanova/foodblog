import React from "react"

import Logo from "./Logo/Logo"
import Menu from "./Menu/Menu"



import './header.css'

    const Header = () => {
       

	return (
      
		<header className="header">
        <div className="container">
            <div className="row header-row">
                <div className="col logo col-xs-9 col-md-6 col-sm-6 col-lg-6">
				<Logo />
                    
                </div>
                <div className="col menu col-xs-3 col-md-6 col-sm-6 col-lg-6">
				<Menu
                 
                />
                
                </div>
            </div>
        </div>
       
    </header>
  
    )
}



export default Header