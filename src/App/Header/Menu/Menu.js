// import React from "react"
import {Link} from 'react-router-dom'
import React, { useState } from "react";

import SideDrawer from ".././SideDrawer";

import MenuIcon from "./MenuIcon";

const Menu = () => {
    const [drawerOpen, setDrawerOpen] = useState(false);
      
    const toggleDrawer = value => {
      setDrawerOpen(value);
    };
    return (
    
        <div>
        <nav className="navigation hidden-menu">
        <ul className="nav-menu" id="nav">
            <li className="current"> <Link to="/" className="active">Home</Link></li>
            <li>  <Link  to="/about" > About</Link></li>
            <li>  <Link  to="/foods" >Foods</Link></li>
            <li>  <Link  to="/gallery" >Gallery</Link></li>
            <li>  <Link  to="/favorites">Favorites</Link></li>
            <li>  <Link  to="/contacts" >Contacts</Link></li>
        </ul>
    </nav>
    <MenuIcon
    onClick={() => toggleDrawer(true)}
    />
    
    <SideDrawer open={drawerOpen} onClose={value => toggleDrawer(value) } />
    </div>
   
    )
}

export default Menu





