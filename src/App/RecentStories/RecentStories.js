import React from "react"
import Title from "./Stories/Title/Title"
import RecentStoriesRow from "./Stories/RecentStoriesRow/RecentStoriesRow"
import RecentStoriesSecondpart from "./Stories/RecentStoriesSecondpart/RecentStoriesSecondpart"
import './recentStories.css'



const RecentStories = () => {

	return (
		<section className="recent-stories" id="#">
			<div id="transcroller-body" className="aos-all">
				<div className="aos-item" data-aos="fade-up">
					<div className="container">
						<div className="row">
							<div className="col col-xs-12 col-md-12 col-sm-12 col-lg-12">

								<Title />

							</div>
						</div>
						<div className="row recent-stories_section">
							<div className="col  recent-stories_part_one col-xs-12 col-md-6 col-sm-12 col-lg-6">

								<RecentStoriesRow />

							</div>
							<div className="col recent-stories_part_two col-xs-12 col-md-6 col-sm-12 col-lg-6">

								<RecentStoriesSecondpart />

							</div>
						</div>
					</div>
				</div>
			</div>
		</section>

	)
}

export default RecentStories 