import React from 'react'
import './storiesList.css'


const StoriesList = ({
    name,
    description,
    image,
})=> {
    return (
        <div>
            <a href="recipe.html">
                <div className="recent-stories_photos">
                    <img src={image} alt="" />
                    <p>{name}</p>   
                    <p>{description}</p> 
                    <div>Read more &#10142; </div>
                </div>
            </a>
        </div>
    )
}
export default StoriesList