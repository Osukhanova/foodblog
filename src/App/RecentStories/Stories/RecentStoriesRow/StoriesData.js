
const storiesData = [
    {
        id: 1,
        name: "Dessert ------- March 01, 2018",
        description: "Tasty & Delicious Foods",
        image: "/images/stories/image_4.jpg",
    },

    {
        id: 2,
        name: "Dessert ------- March 01, 2019",
        description: "Tasty & Delicious Foods",
        image: "/images/stories/image_3.jpg",
    },

    {
        id: 3,
        name: "Dessert ------- March 01, 2020",
        description: "Tasty & Delicious Foods",
        image: "/images/stories/image_1.jpg",
    },

    {
        id: 4,
        name: "Dessert ------- March 01, 2021",
        description: "Tasty & Delicious Foods",
        image: "/images/stories/image_2.jpg",
    }

]

export default storiesData