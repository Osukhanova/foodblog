import React from 'react'
import StoriesList from "./StoriesList/StoriesList"
import  storiesData from "./StoriesData"
import '../../recentStories.css'

const RecentStoriesRow = () => {
    return (
        <div>
            <div className="row recent-stories_row">
   
            {
            storiesData.map(({
              id,
              name,
              description,
              image,
            }) =>(

              <div className="col col-xs-12 col-md-6 col-sm-6 col-lg-6" key={id}>  
                <StoriesList
                    name={name}
                    description={description}
                    image={image}
              />
            </div> 
            ))
          }
            
        </div>
        </div>
    )
}
export default RecentStoriesRow