import React from 'react'
import '../../recentStories.css'
import food from '../Images/image_3.jpg'

const RecentStoriesSecondpart = () => {
    return (
        <div>
            <div className="recent-stories_part_two">
            <div className="recent-stories_secondpart">
                <img src={food} alt="food" />
                <a href="recipe.html">
                    <div className="row-recept-stories">
                        <div className="text">
                            <p>Food</p>
                            <p>Tasty & Delicious Foods</p>
                        </div>
                        <div className="butt-read-more">
                            <a href="recipe.html">
                                <button>Read more &#10142;</button> </a>
                        </div>
                    </div>
                </a>
            </div>
            </div>
        </div>
    )
}
export default RecentStoriesSecondpart