import React from "react"
import { Link } from 'react-router-dom'
import Subscribe from "../../Subscribe/Subscribe"

import '../Gallery/Gallery.css'

import image from '../Gallery/galleryPhotos/bg_4.jpg'
import image2 from '../Gallery/galleryPhotos/bg_5.jpg'
import image3 from '../Gallery/galleryPhotos/home.png'
import image4 from '../Gallery/galleryPhotos/telef.png'
import image5 from '../Gallery/galleryPhotos/letter.png'
import image6 from '../Gallery/galleryPhotos/web.png'

const Contacts = () => {
	return (
        <div>
           <section className="top-post-section" id="#">

           <img src={image} alt="" className="top-post-section-img"/>
        <div id="transcroller-body" className="aos-all">
            <div className="aos-item" data-aos="fade-up">
                <div className="container">
                    <div className="row top-post">

                        <div className="col top-post-block col-xs-12 col-md-12 col-sm-12 col-lg-12">
                            <h1>Contact us</h1>
                            <ul className="bread_crumbs">
                                
                            <li>	<Link to="/"> Home {'>'} </Link></li>
                                <li> Contact us {'>'} </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div className="overlay"></div>
            </div>
        </div>
    </section>

    <section className="contact-section" id="#">
        <div id="transcroller-body" className="aos-all">
            <div className="aos-item" data-aos="fade-up">
                <div className="container">
                    <div className="row conact-us-block">

                        <div className="col col-xs-12 col-md-6 col-sm-12 col-lg-6">
                        <div className="about_us_photo">
                        <img src={image2} alt="" />
                        </div>
                        </div>

                        <div className="col contact-window  col-xs-12 col-md-6 col-sm-12 col-lg-6">

                            <div className="contact-form">

                                <form action="mailto:asukhanova87@gmail.com">
                                    <div className="contacts">
                                        <input type="text" className="" placeholder="Your name"/></div>
                                    <div className="contacts">
                                        <input type="email" className="" placeholder="Your email"/>
                                    </div>
                                    <div className="contacts">
                                        <input type="text" className="" placeholder="Subject"/>
                                    </div>

                                    <div className="textarea-bar">
                                        <textarea className="" placeholder="Your message"></textarea>
                                    </div>

                                    <button className="send_message">Send message </button>

                                </form>

                            </div>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>


    <section className="social-section" id="#">
        <div id="transcroller-body" className="aos-all">
            <div className="aos-item" data-aos="fade-up">
                <div className="container">
                    <div className="row social-info">
                        <div className="col block-circle col-xs-12 col-md-3 col-sm-6 col-lg-3">
                            <div className='circle'>
                            <img src={image3} alt="" />
                            </div>
                            <p>Address</p>
                            <p> 55 Fake St. Fake fake, Toronto, Canada</p>
                        </div>
                        <div className="col block-circle col-xs-12 col-md-3 col-sm-6 col-lg-3">
                            <div className='circle-two'>
                            <img src={image4} alt="" />
                            </div>
                            <p>Contact Number</p>
                            <p><a href="+34 555 555 555"> +34 555 555 555 </a></p>
                        
                        </div>
                        <div className="col  block-circle col-xs-12 col-md-3 col-sm-6 col-lg-3">
                     
                            <div className='circle-three'>
                            <img src={image5} alt="" />
                            </div>
                            <p>Email Address</p>
                            <p><a href="mailto:asukhanova87@gmail.com">info@gmail.com </a></p>
                        
                        </div>
                        <div className="col block-circle col-xs-12 col-md-3 col-sm-6 col-lg-3">
                                <div className='circle-four'>
                                <img src={image6} alt="" />
                                </div>
                            <p>Website</p>
                            <p> <a href="https://olenasukhanova.github.io/FoodBlog/" target="_blank"> yoursite.com </a></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

    <Subscribe/>

       </div>
	)
}

export default Contacts