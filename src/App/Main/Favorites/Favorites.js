import React, { Component } from 'react'
import {keys} from 'lodash'

import { Link } from 'react-router-dom'
import Aim from '../../Aim/Aim'
import Subscribe from '../../Subscribe/Subscribe'
import image from '../Gallery/galleryPhotos/bg_4.jpg'
import photoData from '../Gallery/Photo/photoData'
import FavPhotos from './FavPhotos'

import './favorites.css'



const photoObject = photoData.reduce((accObj, photo) => ({
    ...accObj,
    [photo.id]: photo
}), {})


const Favorites = ({


    addProductToFav,
    photoInFavorites,
    likePhotoState,
	addLike,
    removeLike,
    isLiked,
   
}) => {


    return (
        <div>
           
            <section className="top-post-section" id="#">
                <img src={image} alt="" className="top-post-section-img" />
                <div id="transcroller-body" className="aos-all">
                    <div className="aos-item" data-aos="fade-up">
                        <div className="container">
                            <div className="row top-post">

                                <div className="col top-post-block col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                    <h1> Your favorites : </h1>
                                    <ul className="bread_crumbs">
                                        <li>	<Link to="/"> Home {'>'} </Link></li>
                                        <li> Favorites {'>'} </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div className="overlay"></div>
                    </div>
                </div>
            </section>

            <section className="favorites">
                <div className="container">
                    <div className="">  <p> Thank you for choosing and reading us:</p>  </div>
                    <div className="row favorite-wrap">
                    
                    {
                keys(photoInFavorites).map((photoId) => (
                    <div key={photoId}>{photoObject[photoId].name} - {photoObject[photoId].description} : { <span> &#9825;</span> }
                    </div>

                ))
                    }
                        <FavPhotos
                            addProductToFav={addProductToFav}
                            photoInFavorites={photoInFavorites}
                            addLike={addLike}
                            removeLike={removeLike}
                            isLiked={isLiked}
                        />

                    </div>
                </div>
            </section>

            <Aim />


            <Subscribe />




        </div>
    )
}


export default Favorites