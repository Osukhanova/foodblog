import React from "react"
import { Link } from 'react-router-dom'
import Subscribe from "../../Subscribe/Subscribe"
import AboutMe from "../Gallery/AboutMe"


import image from '../Gallery/galleryPhotos/bg_4.jpg'

const Foods = () => {
	return (
        <div>
           <section class="top-post-section" id="#">
           <img src={image} alt="" className="top-post-section-img"/>
        <div id="transcroller-body" class="aos-all">
            <div class="aos-item" data-aos="fade-up">
                <div class="container">
                    <div class="row top-post">

                        <div class="col top-post-block col-xs-12 col-md-12 col-sm-12 col-lg-12">
                            <h1>Foods</h1>
                            <ul class="bread_crumbs">
                            <li><Link to="/"> Home {'>'} </Link></li>
                                <li> Foods {'>'} </li>
                            </ul>

                        </div>
                    </div>
                </div>
                <div class="overlay"></div>
            </div>
        </div>
    </section>


    <section class="features-posts" id="#">
        <div id="transcroller-body" class="aos-all">
            <div class="aos-item" data-aos="fade-up">
                <div class="container">
                    <div class="row features-posts-section">

                        <div class="col features_part_two col-xs-12 col-md-9 col-sm-12 col-lg-9">
                            <div class="row title-row">
                                <div class="col col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                    <p class="title_part"> My recipes are well-known in Ukraine and in
                                        Canada<span>.</span> <br /> You can also find something intresting for your
                                        home<span>.</span></p>
                                </div>
                            </div>
                            <div class="row recent-stories_section">
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">

                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/image_1.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">
                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/image_2.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                            
                                        </a>
                                    </div>
                                    
                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">

                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/blog-8.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>

                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">

                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/blog-4.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">
                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/blog-5.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">

                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/blog-9.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>

                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">

                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/blog-7.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">
                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/blog-1.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">

                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/image_3.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">

                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/05.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">
                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/bg_5.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'}  Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>

                                </div>
                                <div class="col col-xs-12 col-md-4 col-sm-12 col-lg-4">

                                    <div class="features_photos">
                                        <div class=" features_photo">

                                            <img src="images/blog-2.jpg" alt=""/> </div>
                                        <p> Dessert ------- March 01, 2018</p>
                                        <p>Tasty {'&'} Delicious Recipe</p>
                                        <a href="recipe.html">
                                            <button>Read more &#10142; </button>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="row">
                                <div class="col pagination-row col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                    <div class="pagination">
                                        <a href="#">
                                            <div class="circle-sign">&laquo;</div>
                                        </a>
                                        <a class="active" href="#">1</a>
                                        <a href="#">2</a>
                                        <a href="#">3</a>
                                        <a href="#">4</a>
                                        <a href="#">5</a>
                                        <a href="#">6</a>
                                        <a href="#">&raquo;</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col  col-xs-12 col-md-3 col-sm-12 col-lg-3">
                              <AboutMe/>
                            </div>
                        </div>
                    </div>
                </div>
           
                </div>
       
    </section>
    <Subscribe/>
       </div>
	)
}

export default Foods