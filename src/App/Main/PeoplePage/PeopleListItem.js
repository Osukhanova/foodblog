import React, { Component } from 'react'
import PropTypes from 'prop-types';



class PeopleListItem extends Component {



    render() {
        const {
            id,
            name,
            description,
            image,
        } = this.props;

        return (
            <div>
                <div className="features_photo">
                    <img src={image} alt="" />
                </div>
                <p> {name}  </p>
                <p> {description}</p>

            </div>

        )
    }
}



PeopleListItem.propTypes = {
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    images: PropTypes.string,
}



export default PeopleListItem