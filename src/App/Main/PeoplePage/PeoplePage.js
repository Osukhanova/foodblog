import React from 'react'
import Subscribe from '../../Subscribe/Subscribe'
import AboutMe from '../Gallery/AboutMe'
import PeopleList from './PeopleList'

import '../Gallery/Gallery.css'
import image from '../Gallery/galleryPhotos/bg_4.jpg'

// import photoData, {getPhotoMap} from '../Gallery/Photo/photoData'




const PeoplePage = ({
   match,
//    photoObject = getPhotoMap(photoData) 
}) => {
    // console.log(match)
    //  console.log(photoObject)
    return (
        <div>
            <section className="top-post-section" id="#">
            <img src={image} alt="" className="top-post-section-img"/>
            
                <div id="transcroller-body" className="aos-all">
                    <div className="aos-item" data-aos="fade-up">
                        <div className="container">
                            <div className="row top-post">

                                <div className="col top-post-block col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                    <p> </p>
                                    <h1>People whom I am thankful</h1>
                                    <ul className="bread_crumbs">
                                        <li><a href="index.html"> Home {'>'} </a></li>
                                        <li> People {'>'} </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div className="overlay"></div>
                    </div>
                </div>
            </section>

            <section className="features-posts" id="#">
                <div id="transcroller-body" className="aos-all">
                    <div className="aos-item" data-aos="fade-up">
                        <div className="container">
                            <div className="row features-posts-section">

                                <div className="col features_part_one col-xs-12 col-md-9 col-sm-12 col-lg-9">
                                    <div className="row">
                                        <div className="col col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                            <p className="title_part"> Pleople who have changed my life in Stories<span>.</span></p>
                                        </div>
                                    </div>
                                    <div className="row recent-stories_section">
                                        <PeopleList />
                                    </div>
                                    </div>
                                    <div className="col  col-xs-12 col-md-3 col-sm-12 col-lg-3">
                                        <AboutMe />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <Subscribe />

        </div>

    )
}


export default PeoplePage