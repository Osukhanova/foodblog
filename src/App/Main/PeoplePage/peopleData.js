const peopleData = [
    {
        id:1,
        name:"My first employer",
        description:"Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat aperiam quos recusandae rem non saepe, facere ducimus sequi sunt nobis repellendus expedita delectus quas voluptatibus beatae odit ea vero provident. Lorem ipsum, dolor sit amet consectetur adipisicing elit. Ipsam, corrupti rerum error quia at perspiciatis aut pariatur deleniti doloribus neque ex dignissimos eligendi accusamus nesciunt odit ad ea nisi enim!",
        image:"images/Gallery/person_1.jpg",  
    },
    {
        id:2,
        name:"My Chef",
        description:"Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat aperiam quos recusandae rem non saepe, facere ducimus sequi sunt nobis repellendus expedita delectus quas voluptatibus beatae odit ea vero provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque ea possimus reprehenderit sunt eveniet, fugiat consectetur qui vitae ullam nihil ipsa iure, dolores at numquam sed unde praesentium quod et?",
        image:"images/Gallery/person_2.jpg",
    },
    {
        id:3,
        name:"My friend",
        description:"Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat aperiam quos recusandae rem non saepe, facere ducimus sequi sunt nobis repellendus expedita delectus quas voluptatibus beatae odit ea vero provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque ea possimus reprehenderit sunt eveniet, fugiat consectetur qui vitae ullam nihil ipsa iure, dolores at numquam sed unde praesentium quod et?",
        image:"images/Gallery/person_3.jpg",
    },
    {
        id:4,
        name:"Me college",
        description:"Lorem ipsum dolor sit amet consectetur, adipisicing elit. Placeat aperiam quos recusandae rem non saepe, facere ducimus sequi sunt nobis repellendus expedita delectus quas voluptatibus beatae odit ea vero provident. Lorem ipsum dolor sit amet, consectetur adipisicing elit. Neque ea possimus reprehenderit sunt eveniet, fugiat consectetur qui vitae ullam nihil ipsa iure, dolores at numquam sed unde praesentium quod et?",
        image:"images/Gallery/person_4.jpg",
    },
   
]




export default peopleData