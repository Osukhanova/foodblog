import React from "react"
import PeopleListItem  from './PeopleListItem'
import peopleData from './peopleData'



const PeopleList = () => {

    return (
        <div className="row recent-stories_section">
          

                {
                    peopleData.map(({
                        id,
                        name,
                        description,
                        image,
                    }) => (
                           
                     
                                <div className="col features_photos people-photo col-xs-12 col-md-6 col-sm-12 col-lg-6" key={id}>
                                    <PeopleListItem 
                                    name={name}
                                    description={description}
                                    image={image}
                                    id={id}
                                    />
                                </div>
                              
                         
                        ))
                }
         </div>   
      
    )
}


export default PeopleList