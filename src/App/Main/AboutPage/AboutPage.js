import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import Aim from '../../Aim/Aim'
import Subscribe from '../../Subscribe/Subscribe'


import '../Gallery/Gallery.css'

import image from '../Gallery/galleryPhotos/bg_4.jpg'
import image2 from '../Gallery/galleryPhotos/bg_5.jpg'

class AboutPage extends Component {

    render() {

        return (
            <div>

                <section className="top-post-section" id="#">
                <img src={image} alt="" className="top-post-section-img"/>
                    <div id="transcroller-body" className="aos-all">
                        <div className="aos-item" data-aos="fade-up">
                            <div className="container">
                                <div className="row top-post">

                                    <div className="col top-post-block col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                        <h1>About us</h1>
                                        <ul className="bread_crumbs">
                                        <li>	<Link to="/"> Home {'>'} </Link></li>
                                            <li> About {'>'} </li>
                                        </ul>

                                    </div>
                                </div>
                            </div>
                            <div className="overlay"></div>
                        </div>
                    </div>
                </section>

                <section className="about-section" id="#">
                    <div id="transcroller-body" className="aos-all">
                        <div className="aos-item" data-aos="fade-up">
                            <div className="container">
                                <div className="row about-us-block">

                                    <div className="col col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                        <div className="about_us_photo">
                                        <img src={image2} alt="" />
                                        </div>
                                    </div>

                                    <div className="col about-us-content  col-xs-12 col-md-6 col-sm-6 col-lg-6">

                                        <h2 className="title_part"> About Stories<span>.</span></h2>
                                        <p className="text-left">Far far away, behind the word mountains, far from the countries Vokalia and Consonantia</p>

                                        <div className="row row-about-section">
                                            <div className="col col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                                <div className="square">
                                                    <p> <span className="experience">  </span></p>
                                                    <p>Years of Experienced</p>
                                                    <p>40</p>
                                                </div>
                                            </div>
                                            <div className="col  col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                                <div className="square">
                                                    <p> <span className="foods">  </span></p>
                                                    <p>Foods</p>
                                                    <p>200</p>
                                                </div>
                                            </div>
                                            <div className="col  col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                                <div className="square">
                                                    <p> <span className="photos">  </span></p>
                                                    <p>Photos</p>
                                                    <p>4000</p>
                                                </div>
                                            </div>
                                            <div className="col  col-xs-12 col-md-6 col-sm-6 col-lg-6">
                                                <div className="square">

                                                    <p> <span className="customers">  </span></p>
                                                    <p>Happy Customers</p>
                                                    <p>1500</p>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </section>




                <Aim />


                <Subscribe />




            </div>
        )
    }
}

export default AboutPage