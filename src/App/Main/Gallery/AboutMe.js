import React from "react"

import image from './galleryPhotos/author.jpg'
import image2 from './galleryPhotos/blog-2.jpg'
import image3 from './galleryPhotos/blog-6.jpg'
import image4 from './galleryPhotos/category-2.jpg'
import image5 from './galleryPhotos/icon1.png'


const AboutMe = () => {
    return (
        <div>
       <div className="row side-bar-photos">
                        <div className="col  col-xs-12 col-md-12 col-sm-12 col-lg-12">
                            <div className="side-bar">
                                <h3> About me</h3>
                                <img src={image} alt="Athour"/>
                                <p>Hi! My name is Cathy Deon, behind the word mountains, far from the countries
                                    Vokalia
                                    and Consonantia, there live the blind texts. Separated they live in
                                    Bookmarksgrove
                                    right at the coast of the Semantics, a large language ocean.</p>
                                <div className="search-box">
                                    <form action="">
                                        <input type="text" placeholder="Search" className="search"/>
                                        <button > 
                                        <img src={image5} alt="Search button" className="search_button"/>
                                        </button>
                                    </form>
                                </div>
                            </div>
                        </div>
                        <div className="col  side-bar-links col-xs-12 col-md-12 col-sm-12 col-lg-12">
                            <h3> Categories</h3>

                            <div className="photo">
                                <a href="foods.html">
                                <img src={image2} alt=""/>
                                    <div className="photo-text">
                                        <p>Foods</p>
                                    </div>
                                </a>
                            </div>


                            <div className="photo">
                                <a href="gallery.html">
                                <img src={image3} alt=""/>
                                    <div className="photo-text">
                                        <p>Gallery</p>
                                    </div>
                                </a>
                            </div>


                            <div className="photo">
                                <a href="people.html">  </a>
                                <img src={image4} alt=""/>
                                    <div className="photo-text">
                                        <p>People whom I am thankful</p>
                                    </div>
                               
                            </div>
                            </div>
                        </div>

        </div>



    )
}

export default AboutMe