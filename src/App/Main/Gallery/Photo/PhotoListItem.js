import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import PropTypes from 'prop-types';


class PhotoListItem extends Component {

   renderLike() {
    const {
        id,
        isLiked,
        addLike,
        removeLike,
    } = this.props;
     
       if(isLiked) {
          removeLike(id)
       } else {
          addLike(id)
       }
}
     
      

    render() {
        const {
            id,
            name,
            description,
            image,
            addProductToFav,
            removeProductFromFav,
            isLiked = false,
            renderLike,
        } = this.props;
         
        return (
            <div>
                <div className="features_photo">
                  
                        <img src={image} alt="" /> 

                </div>

                <Link to ={`/photo/${id}`}> 
               <button className="accordion">   See more &#10142; </button> </Link> 
                <h2 className="photo-title"> {name}  </h2>
                <div className="panel">  {description}   </div>
              
                <button className=""
                    onClick={() => isLiked ?  removeProductFromFav (id,description, this.renderLike()) : addProductToFav(id, description, this.renderLike())  }
                > {isLiked ? <span> &#9829;</span> : <span> &#9825;</span>} </button>
            
               

            </div>
        )
    }
}



PhotoListItem.propTypes = {
    name: PropTypes.string.isRequired,
    description: PropTypes.string,
    images: PropTypes.string,
}



export default PhotoListItem 