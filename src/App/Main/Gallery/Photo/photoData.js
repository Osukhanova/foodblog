const photoData = [
    {
        id:1,
        name:"Pizza Recipes",
        description:"This is Pizza Recipes-1",
        image:"images/Gallery/image_6.jpg",
    },
    {
        id:2,
        name:"Pizza Recipes",
        description:"This is Pizza Recipes-2",
        image:"images/Gallery/bg_1.jpg",
    },
    {
        id:3,
        name:"Main Disch Recipes",
        description:"This is Main Disch Recipes-1",
        image:"images/Gallery/image_1.jpg",
    },
    {
        id:4,
        name:"Main Disch Recipes",
        description:"This is Main Disch Recipes-2",
        image:"images/Gallery/image_3.jpg",
    },
    {
        id:5,
        name:"Main Disch Recipes",
        description:"This is Main Disch Recipes-3",
        image:"images/Gallery/image_6.jpg",
    },
    {
        id:6,
        name:"Main Disch Recipes",
        description:"This is Main Disch Recipes-4",
        image:"images/Gallery/blog-4.jpg",
    },
    {
        id:7,
        name:"Desserts",
        description:"This is Desserts-1",
        image:"images/Gallery/bg_2.jpg",
    },
    {
        id:8,
        name:"Desserts",
        description:"This is Desserts-2",
        image:"images/Gallery/05.jpg",
    },
    {
        id:9,
        name:"Desserts",
        description:"This is Desserts-3",
        image:"images/Gallery/image_4.jpg",
    },
    {
        id:10,
        name:"Desserts",
        description:"This is Desserts-4",
        image:"images/Gallery/image_5.jpg",
    },
    {
        id:11,
        name:"Desserts",
        description:"This is Desserts-5",
        image:"images/Gallery/blog-1.jpg",
    },
    {
        id:12,
        name:"Desserts",
        description:"This is Desserts-6",
        image:"images/Gallery/blog-1.jpg",
    },
    {
        id:13,
        name:"Desserts",
        description:"This is Desserts-7",
        image:"images/Gallery/blog-9.jpg",
    },
    {
        id:14,
        name:"Desserts",
        description:"This is Desserts-8",
        image:"images/Gallery/blog-9.jpg",
    },
    {
        id:15,
        name:"Desserts",
        description:"This is Desserts-9",
        image:"images/Gallery/blog-1.jpg",
    },
]




export default photoData