import React from "react"
import PhotoListItem from './PhotoListItem'
import photoData from './photoData'

// import '../Gallery.css'

const PhotoList = ({
    addProductToFav,
    removeProductFromFav,
    likePhotoState,
    addLike,
    removeLike,
}) => {
    return (
        <div className="recent-stories_section">
          

                {
                    photoData.map(({
                        id,
                        name,
                        description,
                        image,
                        
                        
                    }) => (
                            
                     
                                <div className="col features_photos-row  col-xs-12 col-md-6 col-sm-12 col-lg-6" key={id}>
                                    
                                    <PhotoListItem 
                                    name={name}
                                    description={description}
                                    image={image}
                                    id={id}
                                    addProductToFav={addProductToFav}
                                    removeProductFromFav={removeProductFromFav}
                                    isLiked={likePhotoState[id]}
                                    addLike={addLike}
                                    removeLike={removeLike}
                        
                                    />
                                </div>
                              
                         
                        ))
                }
         </div>   
      
    )
}


export default PhotoList