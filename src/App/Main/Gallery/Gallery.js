import React, { Component } from 'react'
import {Link} from 'react-router-dom'
import Subscribe from "../../Subscribe/Subscribe"
import AboutMe from "./AboutMe"
import PhotoList from "./Photo/PhotoList"

import './Gallery.css'

import image from './galleryPhotos/bg_4.jpg'

const Gallery = ({

    likePhotoState,
	addLike,
    removeLike,
    addProductToFav,
    removeProductFromFav,

}) => {

    
    return (
        <div>
            <section className="top-post-section" id="#">
               <img src={image} alt="" className="top-post-section-img"/>
            
                <div id="transcroller-body" className="aos-all">
                    <div className="aos-item" data-aos="fade-up">
                        <div className="container">
                            <div className="row top-post">

                                <div className="col top-post-block col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                    <h1>Gallery</h1>
                                    <ul className="bread_crumbs">
                                        <li>	<Link to="/"> Home {'>'} </Link></li>
                                        <li> Gallery {'>'} </li>
                                    </ul>

                                </div>
                            </div>
                        </div>
                        <div className="overlay"></div>
                    </div>
                </div>
            </section>

            <section className="features-posts" id="#">
                <div id="transcroller-body" className="aos-all">
                    <div className="aos-item" data-aos="fade-up">
                        <div className="container">
                            <div className="row features-posts-section">

                                <div className="col features_part_one col-xs-12 col-md-9 col-sm-12 col-lg-9">
                                    <div className="row">

                                        <div className="col col-xs-12 col-md-12 col-sm-12 col-lg-12">
                                            <p className="title_part"> Gallery posts<span>.</span></p>
                                            <div id="myBtnContainer">
                                                <button className="btn active-btn" onClick="filterSelection('all')"> Show
                                    all</button>
                                                <button className="btn" onClick="filterSelection('pizza')"> Pizza Recipes</button>
                                                <button className="btn" onClick="filterSelection('main-dish')"> Main Dish
                                    Recipes</button>
                                                <button className="btn" onClick="filterSelection('desserts')"> Desserts</button>
                                                 {/* <button className="btn active-btn" onClick={this.reset.bind(this)}> Show
                                    all</button>
                                                <button className="btn" onClick={() => this.sort('pizza')} > Pizza Recipes</button>
                                                <button className="btn" onClick={() => this.sort('desserts')}> Desserts</button> */}
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row recent-stories_section">

                                       <PhotoList 
                                         likePhotoState={likePhotoState}
                                         addLike={addLike}
                                         removeLike={removeLike}
                                         addProductToFav={addProductToFav}
                                         removeProductFromFav={removeProductFromFav}
                                     
                                       />
                                       
                                    </div>
                                </div>


                                <div className="col  col-xs-12 col-md-3 col-sm-12 col-lg-3">
                                    <AboutMe />

                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </section>


            <Subscribe />


        </div>
    )
}


export default Gallery 